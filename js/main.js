$(function() {
  //slick slider init
  $('.js-topslider').slick({
    speed: 2000,
    dots: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 10000,
    fade: true,
    cssEase: 'linear'
  });

  $('.js-slider').slick({
    dots:false,
    arrows:true,
    autoplay: true,
    autoplaySpeed: 50000,
  })

  //fancybox init
  $('[data-fancybox]').fancybox({
    buttons: [
      "zoom",
      "close"
    ],
  });
});